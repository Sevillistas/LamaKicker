# -*- coding: utf-8 -*-
"""
Редактор Spyder

Это временный скриптовый файл.
"""
import pandas as pd

def formatMoneyToFloat(arg):
    money_info = list()
    for i in range(arg.count()):
        if arg[i]=='-' or arg[i].startswith('+-'):
            arg[i]=0
            money_info.append(arg[i])
        else:
            arg[i]=arg[i].replace(',','.')
            money_info.append(arg[i].split(' '))
            money_info[i][0]=float(money_info[i][0])
            if money_info[i][1]=='Th.':
                money_info[i][0]/=1e3
                money_info[i][1]='Mill.'
            arg[i]=money_info[i][0]
            
table = pd.read_csv('1617.csv', sep=',')
table.drop('Club', axis=1, inplace=True)
table.rename(columns={'Club.1':'Club'}, inplace=True)
table.drop('#', 1, inplace=True)
table=table[table.Competition=='LaLiga']
nums = [i for i in range(len(table.Competition=='LaLiga'))]
table.index=nums
formatMoneyToFloat(table.Income)
formatMoneyToFloat(table.Expenditures)
formatMoneyToFloat(table.Balance)
table.to_csv('ready-spain-1617.csv', sep=',', header=True)
