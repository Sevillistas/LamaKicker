from tkinter import *
import rpy2.robjects as robj

def Analyse(team, balance):
	robj.r.source("~/script.R")
	return robj.r.f("runAnalysis({team}, {balance})")


def Predict(event):
	global tbTeam
	global tbBalance
	global lbResult
	lbResult.configure(text=Analyse(tbTeam.get(), int(tbBalance.get())))

mainWindow = Tk()
mainWindow.title("LamaKicker")
mainWindow.geometry("320x240")

# Init frames
inputFrame = Frame(mainWindow, width=200, height=70)
buttonsFrame = Frame(mainWindow, width=200, height=30)
inputFrame.pack(side="top", expand=False)
buttonsFrame.pack(side="bottom", expand=False)

# Init inputs
Label(inputFrame, text="Insert team: ").grid(row=0, column=0, sticky=W)
tbTeam = Entry(inputFrame)
tbTeam.insert(0, "Dabudi")
tbTeam.grid(row=0, column=1)
Label(inputFrame, text="Insert transfer balance, $: ").grid(row=1, column=0, sticky=W)
tbBalance = Entry(inputFrame)
tbBalance.insert(0, "999")
tbBalance.grid(row=1, column=1)
Label(inputFrame, text="Result: ").grid(row=2, column=0, sticky=W)
lbResult = Label(inputFrame)
lbResult.grid(row=2, column=1, sticky=W)

# Init buttons
btnPredict = Button(buttonsFrame, text="Predict")
btnPredict.bind("<Button-1>", Predict)
btnPredict.pack(side="right")

mainWindow.mainloop()